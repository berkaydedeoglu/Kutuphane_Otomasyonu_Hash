#ifndef COMMANDS_H
#define COMMANDS_H

#include "BooksIOHelper.h"
#include "Book.h"
#include <string>

/**
 * Dosyadaki verilerin kullanıcının isteğine göre şekillenmesinin 
 * sağlandığı yani dosyada arama, ekleme, silme gibi işlemlerin 
 * yapıldığı sınıftır.
 **/
class Commands{
 private:
    BooksIOHelper bookFileHelper;
    Bucket tmp_bucket;  // Nesne yönelim mantığına aykırı!!!
    int tmp_bucket_index;  // Nesne yönelim mantığına aykırı!!!

    float packing_factor;
    float avg_search;

    /**
     * Verilen bir hash keyinin dosyada çakışma olmadan hangi kovaya
     * yazılacağını bulan metod.
     *
     * --ARGS--
     *  int hash_key: Yazılacak olan kitap isbn'inin hash fonksiyonu sonucu
     *
     * --RETURN--
     *  int: Yazılabilecek kovanın indexi. Eğer hiçbir yere yazılamazsa bu değer -1 olur.
     **/
    int  find_bucket_to_write(int hash_key);
    // int search_bucket((std::string isbn);
    

 public:

    /**
     * Kitabın isbn numarasına göre arama yapan ve sonucu döndüren metod.
     * 
     * Bazı değişkenleri 'delete_book(std::string)' metodu için sınıf değişkeni olarak 
     * kullanılıyor. Bu modülerliğe aykırı ancak yer ve cpu zamanından tasarruf için 
     * kullanılıyor.
     *
     * -- ARGS--
     *  std::string isbn: Aranan kitabın isbn numarası
     *
     * --RETURN--
     *  Book: Aranan kitap. Eğer bulunamazsa tüm özellikleri boş olan bir kitap nesnesi.
     * 
     **/
    Book find_book(std::string isbn);

    float get_packing_factor();
    float get_avg_search();


    /**
     * Dosyada uygun yere yeni bir kitap eklenmesini sağlayan metod.
     *
     * --ARGS--
     *  Book book: Eklenecek kitap nesnesi.
     *
     * --RETURN--
     *  bool: Kitabın eklenmediği. Kitap eklenebilirse true, eklenemezse false.
     **/
    bool add_book(Book book);

    /**
     * Verilen isbn'e ait kitabın dosyadan mantıksal olarak silinmesini yani
     * state değerinin 'd' yapılmasını sağlayan metod. 
     *
     * Kitap fiziksel olarak silinmez çünkü diğer aramalarda sorun çıkartır.
     *
     * --ARGS--
     *   std::string isbn: Silinecek kitabın isbn numarası
     * 
     * --RETURN--
     *   bool: Kitap silinirse true, silinemezse (ya da bulunamazsa) false.
     **/
    bool delete_book(std::string isbn);

    /**
     * Dosyadaki yoğunlaşma değerini hesaplayan metod.
     * 
     * Yoğunlaşma Değeri = Dosyadaki Kitaplar / Dosyadaki toplam alan
     *
     * --RETURN--
     *  float: Yoğunlaşma değeri
     **/
    void update_statics();

    /**
     * Sınıfın açtığı dosyaları ve akışları güvenli biçimde kaydedip kapatmasını
     * sağlayan metod.
     **/
    void prepare_to_close();
};

#endif
