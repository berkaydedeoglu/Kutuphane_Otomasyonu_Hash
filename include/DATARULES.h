/**
 * Proje boyunca dosya okuma ve yazma işlemlerinin tamamında
 * kullanılacak veri kurallarının global tanımlarını içeren
 * header dosyası.
 *
 * Yazar: Berkay Dedeoğlu
 * Tarih: 27.02.2018
 * 
 **/


#ifndef DATARULES_H
#define DATARULES_H

#include <string>


// Başlangıçta hazır olarak gelen kitap dosyasının (plain text) yolu ve ismi.  
const std::string TEXTFILENAME = "../data/Books.txt";


// Verilerin tutulduğu ve üzerinde işlemlerin yapıldıığı (binary) dosyanın yolu ve ismi.
const std::string DATAFILENAME = "../data/Books.bin";


// Kitap kayıtlarının sabit uzunluklu olması için kullanılan boş karakter. bkz. Book::serialize()
const char BLANK_CHAR = 1;


// Verilerin tutulduğu dosyadaki sabit uzunlukları
const int STATE_SIZE = 1;  // byte
const int ISBN_SIZE = 20;  // byte
const int NAME_SIZE = 200;  // byte
const int AUTHOR_SIZE = 100;  // byte
const int COST_SIZE = 10;  // byte

const int BOOK_SIZE = 331; // byte

// Dosyadaki bir kovanın toplam boyutu
const int BUCKET_SIZE = 1655; // byte

// Dosyada bulunması gereken kova sayısı
const int BUCKET_COUNT = 41;

// Bir kovadaki kitap sayısı
const int COLUMN_COUNT = 5;

#endif

