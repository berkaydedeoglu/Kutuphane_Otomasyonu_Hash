/**
 * Yazar: Berkay Dedeoğlu
 * Tarih: 01.03.2018
 **/


#ifndef BOOKSIOHELPER_H
#define BOOKSIOHELPER_H

#include <fstream>
#include <string>
#include "Book.h"
#include "Bucket.h"

/**
 * Kitap verilerinin tutulduğu binary dosyaya erişimi sağlayan
 * sınıftır.
 *
 * Proje boyunca ikili dosyaya sadece buradan erişim sağlanır.
 * Dosya bu sınıf ile projenin diğer modüllerinden 
 * soyutlanmıştır.
 *
 * Sınıfla sadece Bucket nesneleri kullanarak iletişim kurabilirsiniz.
 **/
class BooksIOHelper{

 private:
  std::fstream book_file;
  unsigned int total_book_count;
  int total_collusion;

  /**
   * Dosya işaretçisini, verilen kitap verisinin
   * başına koyan metod.
   *
   * --Input--
   *   unsigned int bucket: Gidilmesi istenen kovanın sıra numarası
   *   unsigned int book_index: Gidilmesi istenen kitabın kova içindeki sıra numarası 
   **/
  void go_record(unsigned int bucket_index,
		 unsigned int book_index);

  
  /**
   * Dosya işaretçisini verilen kova verisinin başına
   * koyan metod.
   *
   * --ARGS--
   *  unsigned int bucket_index: Gidilmek istenen kovanın sıra numarası
   **/
  void go_bucket(unsigned int bucket_index);

 public:

  /**
   * Sadece özellikler ilklendirilir.
   **/
  BooksIOHelper();


  /**
   * Binary veri dosyasını açan metod.
   *
   * --Return--
   *    bool: Dosyanın açılıp açılamadığı durumu. 
   **/
  bool open_file();


  /**
   * Dosyadan istenilen sıradaki veriyi alıp Bucket 
   * nesnesine dönüştüren metoddur.
   *
   * Verinin silinmiş olması bu metodu etkilemez.
   *
   * --Input--
   *   int index: İstenilen kovanın indexsi.
   *
   * --Return--
   *   Book: İstenilen sıradaki verinin Bucket nesnesi. 
   **/
  Bucket get_bucket(unsigned int index);

  
  /**
   * Veri dosyasındaki tutulması gereken istatistikleri güncelleyen metod
   **/
  void update_statics();

  /**
   * Dosyada verilen index üzerindeki kaydın 
   * üzerine yazarak yeni kayıt oluşturan metod.
   *
   * Güncelleme yapmak ya da silinen veri üzerine 
   * yazmak için kullanılabilir.
   *
   * --ARGS--
   *   Bucket bucket: Yazılması istenen kova nesnesi
   *   index: Üzerine yazılan kovanın dosya indeksi.
   **/
  void write_bucket(Bucket bucket, unsigned int index);


  /**
   * Dosya üzerindeki indexi verilen kaydın durum değerini
   * değiştiren metod.
   *
   * Gerçek anlamda diskten silme işlemi yapmaz. Durum değerini
   * 'd' (deleted) yapar.
   *
   * --ARGS--
   *   unsigned int bucket_index: İstenilen kitabın bulunduğu kovanın sıra numarası
   *   unsigned int book_index: İstenilen kitabın kovadaki indexi
   **/
  void delete_book(unsigned int bucket_index,
		   unsigned int book_index);
  

  /**
   * Tamponda bekleyen veriyi fiziksel belleğe aktaran 
   * bilinen tanımıyla kaydeden method. 
   *
   * flush() metodunun soyutlanmış halidir.
   **/
  void save_file();

  int get_total_book_count();
  int get_total_collusion();

  /**
   * Dosyanın kapatılması ve sıkıştırılmasını sağlayan metod.
   **/
  void close_file();
};
#endif
