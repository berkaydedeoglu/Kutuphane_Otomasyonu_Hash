#ifndef BUCKET_H
#define BUCKET_H

#include "Book.h"
#include <string>
#include "DATARULES.h"

class Bucket{
  private:
    /**
     * İçinde 5 tane kitap bulunan dizi. Sınıfın amacı bu 
     * sanal tutucuları oluşturup tampon oluşturmaktır.
     **/ 
    Book bucket[5];

  public:
    Bucket();
    Bucket(Book books[5]);

    /**
     * Lineer arama ile istenilen kitabı bulma metodu.
     * 
     * --ARGS--
     *  std::string isbn: İstenilen kitabın isbn değeri.
     *
     * --RETURN--
     *  Book: istenilen isbn'e sahip kitap nesnesi. Eğer aranan kitap yoksa boş 
     *        kitap nesnesi döndürülür.
     **/ 
    Book find_book(std::string isbn);

    /**
     * Kovanın tammen dolu olup olmadığını kontrol eden metod.
     * Silinmiş öğeler de sayılır.
     * 
     * --RETURN--
     *  bool: Kova doluysa true, en az bir boşluk varsa false
     **/
    bool is_full();

    /**
     * Sınıfı dosyaya yazmaya hazırlayan metod. Aslında sadece içindeki kitapların 
     * serialize() metodlarını kullanıp toplayan metod.
     *
     * --RETURN--
     *  std::string: Kovanın dosyaya yazılabilir hali.
     **/
    std::string serialize();

    /**
     * Serileştirilmiş stringten yani dosyadan verileri alıp,
     * onları ayrıştırıp kovanın gerekli yerlerine yazan metod.
     *
     * --ARGS--
     *  std::string: Serileştirilmiş veri. Genelde dosyadan alınır.
     **/
    void deserialize(std::string);

    /**
     * Kovadaki istenilen indexteki verinin gerçek bir kitap mı yoksa 
     * boşluk mu olduğunun kontrolünü yapan metod.
     *
     * --ARGS--
     *  unsigned int index: Kontrol edilecek kitabın kova sırası
     *
     * --RETURN--
     *   bool: Verinin boş ise true, gerçek kitap ise false 
     **/
    bool is_this_index_null(unsigned int index);

    /**
     * Kovadaki istenilen indexteki verinin silinip silinmediğinin kontrolünü
     * yapan metod.
     *
     * --ARGS--
     * unsigned int index: Kontrol edilecek kitabın kova sıras
     *
     * --RETURN--
     * bool: Verinin silinmiş ise true, gerçek kitap ise false
     **/
    bool is_this_index_deleted(unsigned int index);

    /**
     * Kovada en az bir alanın boş ya da silinmiş olduğunun kontrlünü yapan
     * metod.
     *
     * --RETURN--
     *  bool: İstenilen şartlar sağlanırsa true, sağlanmazsa false.
     **/
    bool is_empty_or_any_deleted();
    

    Book get_book(unsigned int index);
    void set_book(unsigned int index, Book book);
};

#endif
