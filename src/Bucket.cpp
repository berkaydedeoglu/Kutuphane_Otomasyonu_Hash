#include "../include/Bucket.h"
#include <iostream>

Bucket::Bucket(){

}

Bucket::Bucket(Book books[5]){
    for (int i=0; i<5; i++){this->bucket[i] = books[i];}
}

Book Bucket::find_book(std::string isbn){
    int i = 0;
    for (i; i<5; i++){
	if (isbn == bucket[i].get_isbn())
	    return bucket[i];
    }

    Book nullBook;
    return nullBook;
}

bool Bucket::is_full(){
    // Todo
    bool flag = true;
    for (int i=0; i<5; i++){
	if (is_this_index_null(i)){
	    flag = false;
	    break;
	}
    }
    return flag;
}

bool Bucket::is_empty_or_any_deleted(){

    bool flag = false;
    for (int i=0; i<5; i++){
	if (is_this_index_null(i) || is_this_index_deleted(i)){
	    flag = true;
	    break;
	}
    }
    return flag;
}

std::string Bucket::serialize(){
    std::string serial = "";

    for (int i=0; i<5; i++){
	serial += bucket[i].serialize();
    }

    return serial;
}

void Bucket::deserialize(std::string serialized){
    for (int i=0; i<5; i++){
	std::string serializedBook = serialized.substr(i*BOOK_SIZE,
						       BOOK_SIZE);
	
	Book tmpBook;
	tmpBook.deserialize(serializedBook);
	bucket[i] = tmpBook;
    }
}

bool Bucket::is_this_index_null(unsigned int index){
    if (bucket[index].get_isbn() == "")
	return true;
    else
	return false;
}

bool Bucket::is_this_index_deleted(unsigned int index){
    if (bucket[index].get_state() == "d")
	return true;
    else
	return false;
}

Book Bucket::get_book(unsigned int index){
    if (index >= 5){Book nullBook; return nullBook;}
    else{return bucket[index];}
}

void Bucket::set_book(unsigned int index, Book book){
    if (index >= 5){return;}
    else{this->bucket[index] = book;}
}
