/**
 * İkili dosyaya erişimi diğer sınıflardan soyutlayan sınıf.
 * 
 * Detaylı bilgi BooksIOHelper.h dosyasındadır.
 *
**/

#include "../include/BooksIOHelper.h"
#include "../include/DATARULES.h"
#include <string>

BooksIOHelper::BooksIOHelper(){
      this->open_file();
      update_statics();
}

int BooksIOHelper::get_total_book_count() {return total_book_count;}
int BooksIOHelper::get_total_collusion() {return total_collusion;}

bool BooksIOHelper::open_file(){

    this->book_file.open(DATAFILENAME,
			 std::ios::binary | std::ios::out |
			 std::ios::in | std::ios::ate);

   
    if (book_file.is_open()){ 
        return true;
    }
    else return false;
}

void BooksIOHelper::go_record(unsigned int bucket_index,
			      unsigned int book_index){
    int record_byte;
    record_byte = (bucket_index * BUCKET_SIZE) +
	(book_index * BOOK_SIZE);

    if (book_index < 5 && bucket_index < BUCKET_COUNT)
        book_file.seekg(record_byte);
}

void BooksIOHelper::go_bucket(unsigned int bucket_index){
    int bucket_byte;
    bucket_byte = bucket_index * BUCKET_SIZE;

    if (bucket_index < BUCKET_COUNT){
	book_file.seekg(bucket_byte);
    }
}

Bucket BooksIOHelper::get_bucket(unsigned int index){

    char* data_str = new char[BUCKET_SIZE];
    Bucket bucket;
  
    this->go_bucket(index);

    book_file.read(data_str, BUCKET_SIZE);
    bucket.deserialize(data_str);

    delete[] data_str;
    return bucket;
}

void BooksIOHelper::update_statics(){
    unsigned int book_count = 0;
    total_collusion=0;
    total_book_count=0;

    for (int i = 0; i<BUCKET_COUNT; i++){
	Bucket bucket = this->get_bucket(i);
	for (int j = 0; j < 5; j++){
	    Book book = bucket.get_book(j); 
	    if (book.get_isbn() != ""){
		book_count++;
		int h = Book::hash(book.get_isbn());

		if (h <= i){
		    this->total_collusion += 1+ (i-h);
		}else{
		    this->total_collusion += 42 - ( h - i);
		}
		
	    }
	}
    }
    this->total_book_count = book_count;
}

void BooksIOHelper::write_bucket(Bucket bucket, unsigned int index){
    go_bucket(index);
    book_file << bucket.serialize();
    save_file();
}

void BooksIOHelper::delete_book(unsigned int bucket_index,
				unsigned int book_index){
    go_record(bucket_index, book_index);
    book_file << 'd';
    save_file();
}

void BooksIOHelper::save_file(){
    book_file.flush();
}

void BooksIOHelper::close_file(){
  //this->compress_file();
  book_file.close();
}

