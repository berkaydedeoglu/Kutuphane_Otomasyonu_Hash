dosya_yolu = "../data/Books.bin"
bir_kitap_boyutu = 331
iba = 1
ibi = 21


with open(dosya_yolu, "rb") as file:
    by = file.read(bir_kitap_boyutu)
    ks = 0
    gks = 0
    while by:
        by = by.decode()
        durum = by[0:1]
        by = by[iba:ibi].strip(chr(1))

        if (ks%5 == 0):
            print("====== KOVA {} ========".format(int(ks/5)))
        try:

            isbn = int(by)
            gks += 1
            print("{}. - isbn numarası: {} - hash: {} - durum: {}".format((ks%5), by, isbn%41, durum))
        except:
            print("{}. --------------------".format(ks%5))

        by = file.read(bir_kitap_boyutu)
        ks += 1
    print(gks)
