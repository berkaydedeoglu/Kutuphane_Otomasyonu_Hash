#include "../include/Book.h"
#include "../include/Bucket.h"
#include "../include/PlaintoBinary.h"
#include "../include/BooksIOHelper.h"
#include "../include/Commands.h"
#include <iostream>
#include <string>
#include <climits>


Commands commands;

int print_menu();
void delete_book();
void add_book();
void find_book();


int main(int argc, char *argv[]){
    PlaintoBinary ptb;

    ptb.write_to_data_file();

    commands.update_statics();
    while (true){
	int choice = print_menu();

	switch(choice){
	case 1:
	    add_book();
	    break;
	case 2:
	    find_book();
	    break;
	case 3:
	    delete_book();
	    break;
	case 4:
	    commands.prepare_to_close();
	    return 1;
	default:
	    std::cout << "Yanlış Tuşlama" << std::endl;
	    break;
	}

	// Devam etmek için bir tuşa basın komutları
	char temp;
	std::cin.ignore();
	std::cout << std::endl << std::endl << std::endl << std::endl;
	std::cout << "Devam etmek için bir <Enter> basın :)"; 
	std::cin.getline(&temp, 1);
	std::cin.clear();
	// Devam etmek için bir tuşa basın komutla

    }

    commands.prepare_to_close();

    return 0;
}


int print_menu(){
    std::cout << "=======================" << std::endl;
    std::cout << "1-) Kitap ekleme" << std::endl;
    std::cout << "2-) Kitap arama" << std::endl;
    std::cout << "3-) Kitap silme" << std::endl;
    std::cout << "4-) Çıkış" << std::endl;


    std::string choice;
    std::cin >> choice;

    std::cout << std::endl << std::endl;

    return std::stoi(choice);
}

void add_book(){
    std::cin.ignore();

    std:: cout << "ISBN: ";
    std::string isbn;
    std::getline(std::cin,isbn);
    
    std::cout << "Kitap adı: ";
    std::string name;
    std::getline(std::cin, name);

    std::cout << "Yazar adı: ";
    std::string author;
    std::getline(std::cin, author);

    std::cout << "Kitap fiyatı: ";
    std::string cost;
    std::getline(std::cin, cost);

    std::cin.clear();

    Book book(isbn, name, author, cost);
    bool result = commands.add_book(book);

    if (result == true){std::cout << "Kitap başarıyla eklendi" << std::endl;}
    else{std::cout << "Bir hata oluştu ve kitabınız eklenemedi. Belki de dosya kapasitesi dolmuştur." << std::endl;}

    commands.update_statics();
    float pf = commands.get_packing_factor();
    if (pf > 0.1){
	std::cout << "Dosyada yeriniz azalıyor. Lütfen kontrol edin." << std::endl;
	std::cout << "Packing factor: " + std::to_string(pf) << std::endl; 
    }
}


void find_book(){
    std::string isbn;
    std::cout << "Bulmak istediğiniz kitabın isbn kodu: ";
    std::cin >> isbn;

    Book book = commands.find_book(isbn);

    if (book.get_isbn() != ""){
	std::cout << "\n";
	std::cout << "=========="<<std::endl;
	std::cout << "Kitap adı: " + book.get_name() << std::endl;
	std::cout << "Yazar adı: " + book.get_author() << std::endl;
	std::cout << "Kitap tutarı: $" + book.get_cost() << std::endl;
    }else
	std::cout << "Aradığınız kitap yok ya da silinmiş" << std::endl;

    if (commands.get_avg_search() >= 1.5){
	std::cout << "Ortalama arama değeri yükseldi. Değer: ";	
	std::cout << std::to_string(commands.get_avg_search()) << std::endl;
    }
}

void delete_book(){
    std::string isbn;
    std::cout << "Silmek istediğiniz kitabın isbn kodu: ";
    std::cin >> isbn;
    bool result = commands.delete_book(isbn);

    if (result == true)
	std::cout << "Kitap başarıyla silindi" << std::endl;
    else
	std::cout << "İstediğiniz kitap yok ya da silinmiş" << std::endl;
}
