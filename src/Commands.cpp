#include "../include/Commands.h"
#include "../include/DATARULES.h"
#include "../include/Bucket.h"
#include <iostream>

int Commands::find_bucket_to_write(int hash_key){
    int address = hash_key;
    int limit = BUCKET_COUNT - 1;
    Bucket bucket;

    for (address; address<=limit; address++){
	bucket  = bookFileHelper.get_bucket(address);

	if (bucket.is_empty_or_any_deleted()){
	    return address;
	}

	if (address == (BUCKET_COUNT-1)){ // Daha optimize çalışması sağlanabilir.
	    address  = -1; // Modülo işlemi düşünülebilir
	    limit = hash_key;
	}  
    }

    return -1;
}

float Commands::get_packing_factor(){return packing_factor;}
float Commands::get_avg_search(){return avg_search;}


Book Commands::find_book(std::string isbn){
    tmp_bucket_index = Book::hash(isbn);
    register int limit = BUCKET_COUNT-1;

    for (tmp_bucket_index; tmp_bucket_index<=limit; tmp_bucket_index++){
	
	tmp_bucket = bookFileHelper.get_bucket(tmp_bucket_index);
	Book t = tmp_bucket.find_book(isbn);

	if (t.get_isbn() != "" && t.get_state() != "d")
	    return t;
	
	if (!tmp_bucket.is_full())
	    break;
	
	if (tmp_bucket_index == BUCKET_COUNT - 1){
	    limit = Book::hash(isbn);
	    tmp_bucket_index = -1;
	}
    }
    
    Book nullBook;
    return nullBook;
}

bool Commands::add_book(Book book){
    if (find_book(book.get_isbn()).get_isbn() != "")
        return false;
    
    int hash_key = Book::hash(book.get_isbn());
    int bucket_address = this->find_bucket_to_write(hash_key);

    if (bucket_address < 0) {return false;}
    else{
	Bucket bucket = bookFileHelper.get_bucket(bucket_address);

	for (int i = 0; i < 5; i++){
	    if (bucket.is_this_index_null(i) || bucket.is_this_index_deleted(i)){
		bucket.set_book(i, book);
		break;
	    }
	}

	bookFileHelper.write_bucket(bucket, bucket_address);
	return true;
    }
}

bool Commands::delete_book(std::string isbn){
    Book book2 = this->find_book(isbn);

    if (book2.get_isbn() == "")
	return false;
    else{
	int index  = 0;

	for (index; index < 5; index++){
	    if (tmp_bucket.get_book(index).get_isbn() == isbn){
		bookFileHelper.delete_book(tmp_bucket_index, index);
		return true;
	    }
	}
	return false;
    }
}

void Commands::update_statics(){
    
    bookFileHelper.update_statics();
    packing_factor = (float) bookFileHelper.get_total_book_count() / (float)(BUCKET_COUNT*5);
    
    avg_search = (float) bookFileHelper.get_total_collusion() / (float) bookFileHelper.get_total_book_count();
    
}


void Commands::prepare_to_close(){
    bookFileHelper.close_file();
}
